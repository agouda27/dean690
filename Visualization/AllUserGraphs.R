Orders <- read.csv('orders.csv',header = TRUE,sep = ",")

############ Day of Week #######################
dow <- Orders %>%
  group_by(order_dow) %>%
  summarize(n())

ylab <- c(200,400,600)
plot_dow <-ggplot(dow, aes(x=order_dow, y=dow$`n()`),values = c('orange')) +
  geom_bar(stat="identity",width = 0.5, fill = "#FF6666" ) +
  scale_x_continuous(breaks= c(0,1,2,3,4,5,6)) + 
  scale_y_continuous(labels = paste0(ylab, "K"),
                     breaks = 10^3 * ylab) + 
  labs(title="Orders Distribution of the Week for All Users", 
       x="Day of Week ", y = "Number of Users") + 
  theme_update(plot.title = element_text(hjust = 0.5))
plot_dow


############# Hour of the Day #######################
hod <- Orders %>%
  group_by(order_hour_of_day) %>%
  summarize(n())

ylab <- c(100,200,300)
plot_hod <-ggplot(hod, aes(x=order_hour_of_day, y=hod$`n()`),values = c('orange')) +
  geom_bar(stat="identity",width = 0.5, fill = "#FF6666" ) +
  scale_x_continuous(limits =  c(-1,24)) + 
  scale_y_continuous(labels = paste0(ylab, "K"),
                     breaks = 10^3 * ylab) + 
  labs(title="Orders Distribution of Hours for All Users", 
       x="Hour of Day ", y = "Number of Users")
plot_hod

######To Plot Average Days Since Prior Order#######
DaysincePrior <- Orders %>%
  group_by(user_id) %>%
  summarize( average_interval_time = mean(days_since_prior_order, na.rm = TRUE))
head(DaysincePrior)

plot_dayprior <-ggplot(DaysincePrior, aes(x=average_interval_time)) +
  geom_histogram (aes(y=..density..),color = 'black',fill = '#FF6666',binwidth = 2) +
  geom_density(size=.7, color = "black")+
  scale_x_continuous(limits = c(-1,31)) + 
  labs(title="Average Time Between Orders for All Users", 
       x="Days", y = "Density")
plot_dayprior
plot_dayprior <- plot_dayprior + geom_vline(aes(xintercept=10),
                                            color="black", linetype="dashed", size=1)
plot_dayprior <- plot_dayprior + geom_vline(aes(xintercept=15.2),
                                            color="black", linetype="dashed", size=1)
plot_dayprior

#########Orders Placed Histogram##########
Orders_Group <- aggregate(order_number ~ user_id, data = Orders, max)
ylab <- c(5, 15, 25, 35, 45)
ordersHist <- ggplot(Orders_Group, aes(x=order_number)) +
  geom_histogram(fill = "#FF6666", binwidth = 2) +
  scale_y_continuous(labels = paste0(ylab, "K"),
                     breaks = 10^3 * ylab) + 
  labs(title="Frequency Histogram of Total Orders Placed", 
       x="Number of Total Orders Placed", y = "Count of Users")
ordersHist
