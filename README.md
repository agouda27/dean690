# dean690

Capstone project course for MS in Data Analytics program. Key activity is completion of a major applied team project resulting in an acceptable technical report and oral briefing.

All the file dependencies required for the scripts are uploaded on google drive due to size limits here:
https://drive.google.com/drive/folders/1lM95t_-AEu1YNypBEk2WL7fh9zx0_kqB?usp=sharing
